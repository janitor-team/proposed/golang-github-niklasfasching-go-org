Source: golang-github-niklasfasching-go-org
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any (>= 2:1.16~),
               golang-github-alecthomas-chroma-dev (>= 0.8.2),
               golang-github-pmezard-go-difflib-dev (>= 1.0.0),
               golang-golang-x-net-dev (>= 1:0.0+git20201224.6772e93)
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-niklasfasching-go-org
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-niklasfasching-go-org.git
Homepage: https://github.com/niklasfasching/go-org
XS-Go-Import-Path: github.com/niklasfasching/go-org

Package: golang-github-niklasfasching-go-org-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-alecthomas-chroma-dev (>= 0.8.2),
         golang-github-pmezard-go-difflib-dev (>= 1.0.0),
         golang-golang-x-net-dev (>= 1:0.0+git20201224.6772e93),
         ${misc:Depends}
Description: Org mode parser with HTML & pretty-printed Org rendering (Go library)
 github.com/niklasfasching/go-org an Org mode parser written in Go.
 .
 Take a look at https://niklasfasching.github.io/go-org/ for some examples
 and an online Org → HTML demo (Wasm based).
 .
 Please note that the goal for the HTML export is to produce sensible HTML
 output, not to exactly reproduce output the output of org-html-export.
 .
 This package contains the go-org Go library.

Package: go-org
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Org mode parser with HTML & pretty-printed Org rendering (program)
 go-org is an Org mode parser written in Go.
 .
 Take a look at https://niklasfasching.github.io/go-org/ for some examples
 and an online Org → HTML demo (Wasm based).
 .
 Please note that the goal for the HTML export is to produce sensible HTML
 output, not to exactly reproduce output the output of org-html-export.
 .
 This package contains the go-org executable.
